package com.example.gaticosapi_agustinkohan


import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.gaticosapi_agustinkohan.data.model.Breeds
import com.example.gaticosapi_agustinkohan.ui.theme.Typography
import com.example.gaticosapi_agustinkohan.ui.theme.GaticosAPI_AgustinKohanTheme
import com.skydoves.landscapist.ImageOptions
import com.skydoves.landscapist.glide.GlideImage
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json


class CatDetail : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val intent=intent
        val catInfoStr=intent.getStringExtra("CatInfo")
        val catInfo=catInfoStr?.let { Json.decodeFromString<Breeds>(it) }
        setContent {
            GaticosAPI_AgustinKohanTheme() {
                // A surface container using the 'background' color from the theme
                /* Surface(
                     modifier = Modifier.fillMaxSize(),
                     color = MaterialTheme.colors.background
                 ) {  }*/
                Column(
                    modifier = Modifier
                        .fillMaxSize()
                        .background(color = MaterialTheme.colors.background)
                        .padding(5.dp)
                        .verticalScroll(rememberScrollState()),
                    verticalArrangement = Arrangement.Center,
                    horizontalAlignment = Alignment.CenterHorizontally,

                    ) {
                    if (catInfo != null) {
                        FullCatCard(catInfo)
                    }
                }
            }
        }
    }
}

@Composable
fun FullCatCard(catInfo: Breeds) {
    Card(
        border = BorderStroke(
            4.dp,
            color = MaterialTheme.colors.primary
        ),
        elevation = 5.dp,
        backgroundColor = MaterialTheme.colors.secondary) {
        Column(horizontalAlignment = Alignment.CenterHorizontally) {
            GlideImage(

                imageModel = { catInfo.image!!.url }, modifier = Modifier
                    .padding(top = 24.dp)
                    .size(200.dp)
                    .border(BorderStroke(4.dp, MaterialTheme.colors.primary)),
                imageOptions = ImageOptions(contentScale = ContentScale.Crop),
            )
            Description(catInfo)
        }
    }
}

@Composable
fun Description(catInfo: Breeds) {
    Column(modifier = Modifier.padding(4.dp)) {
        Spacer(modifier = Modifier.height(10.dp))
        Text(
            text = "Description: ",
            color = MaterialTheme.colors.primary,
            style = Typography.h4,
            modifier = Modifier
                .padding(8.dp)
        )
        Divider(color = MaterialTheme.colors.primary, thickness = 1.dp, modifier = Modifier.padding(start = 8.dp, end = 20.dp))
        Spacer(modifier = Modifier.height(10.dp))
        catInfo.description?.let {
            Text(
                text = it,
                color = MaterialTheme.colors.primaryVariant,
                modifier = Modifier
                    .padding(start = 15.dp, end = 15.dp),
                textAlign = TextAlign.Justify
            )
        }
        Spacer(modifier = Modifier.height(10.dp))

        Text(
            text = "Breeds: ",
            color = MaterialTheme.colors.primary,
            style = Typography.h4,
            modifier = Modifier
                .padding(8.dp)
        )
        Divider(color = MaterialTheme.colors.primary, thickness = 1.dp, modifier = Modifier.padding(start = 8.dp, end = 20.dp))

        Spacer(modifier = Modifier.height(10.dp))
        catInfo.name?.let {
            Text(
                text = it,
                color = MaterialTheme.colors.primaryVariant,
                modifier = Modifier
                    .padding(start = 15.dp, end = 15.dp),
                textAlign = TextAlign.Justify
            )
        }
        Spacer(modifier = Modifier.height(10.dp))
        Text(
            text = "Temperament: ",
            color = MaterialTheme.colors.primary,
            style = Typography.h4,
            modifier = Modifier
                .padding(8.dp)
        )
        Divider(color = MaterialTheme.colors.primary, thickness = 1.dp, modifier = Modifier.padding(start = 8.dp, end = 20.dp))

        Spacer(modifier = Modifier.height(10.dp))
        catInfo.temperament?.let {
            Text(
                text = it,
                color = MaterialTheme.colors.primaryVariant,
                modifier = Modifier
                    .padding(start = 15.dp, end = 15.dp),
                textAlign = TextAlign.Justify
            )
        }

        Spacer(modifier = Modifier.height(10.dp))
        Text(
            text = "Wiki: ",
            color = MaterialTheme.colors.primary,
            style = Typography.h6,
            modifier = Modifier
                .padding(8.dp)
        )
        Divider(color = MaterialTheme.colors.primary, thickness = 1.dp, modifier = Modifier.padding(start = 8.dp, end = 20.dp))

        Spacer(modifier = Modifier.height(10.dp))
        catInfo.wikipediaUrl?.let {
            ButtonWiki(Url = catInfo.wikipediaUrl!!)
        }
        Spacer(modifier = Modifier.height(10.dp))
    }
}
@Composable
fun ButtonWiki(Url:String){
    val context= LocalContext.current
    val intent= remember {
        Intent(Intent.ACTION_VIEW, Uri.parse(Url))
    }
    Box(modifier = Modifier.clickable { context.startActivity(intent) }){
        Text(
            text = Url,
            color = MaterialTheme.colors.primaryVariant,
            modifier = Modifier
                .padding(start = 15.dp, end = 15.dp),
            textAlign = TextAlign.Justify
        )
    }
}

