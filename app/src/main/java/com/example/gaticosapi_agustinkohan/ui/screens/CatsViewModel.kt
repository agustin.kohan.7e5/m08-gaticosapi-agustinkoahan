package com.example.gaticosapi_agustinkohan.ui.screens

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.gaticosapi_agustinkohan.data.model.Breeds
import com.example.gaticosapi_agustinkohan.network.CatsApi
import com.example.gaticosapi_agustinkohan.data.model.CatImage
import kotlinx.coroutines.launch
import retrofit2.HttpException
import java.io.IOException

sealed interface CatsUiState {
    data class Success(val breeds:List<Breeds>, val SelectCountry: String): CatsUiState
    object Error : CatsUiState
    object Loading : CatsUiState
}

class CatsViewModel: ViewModel() {
    var catsUiState: CatsUiState by mutableStateOf(CatsUiState.Loading)
        private set

    init {
        getCatsPhotos()
    }

    private fun getCatsPhotos() {
        viewModelScope.launch {
            catsUiState=try {
                val listResult = CatsApi.retrofitService.getPhotos()
                CatsUiState.Success(listResult, "SelectCountry")
            } catch (e: IOException){
                CatsUiState.Error
            } catch (e: HttpException){
                CatsUiState.Error
            }
        }
    }

    fun SetSelectCountry(newCountry: String){
        if(catsUiState is CatsUiState.Success){
            catsUiState=(catsUiState as CatsUiState.Success).copy(SelectCountry = newCountry)
        }
    }


}