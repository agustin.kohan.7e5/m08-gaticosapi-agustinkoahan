package com.example.gaticosapi_agustinkohan.ui

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.gaticosapi_agustinkohan.R
import com.example.gaticosapi_agustinkohan.ui.screens.CatsViewModel
import com.example.gaticosapi_agustinkohan.ui.screens.MainScreen


@Composable
fun CatImageApp(modifier: Modifier = Modifier) {
    Scaffold(
        modifier = modifier.fillMaxSize(),
        topBar = { TopAppBar(title= {Text(stringResource(R.string.app_name)) })}
    ) {
        Surface(
            modifier = Modifier
                .fillMaxSize()
                .padding(it),
            color = MaterialTheme.colors.background
        ) {
            val catsViewModel: CatsViewModel= viewModel()
            MainScreen(catsUiState = catsViewModel.catsUiState,
                setSelectCountry = {catsViewModel.SetSelectCountry(it)},
                modifier = Modifier.background(color = MaterialTheme.colors.primaryVariant)
                )

        }
    }
}
