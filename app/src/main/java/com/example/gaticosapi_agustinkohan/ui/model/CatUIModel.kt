package com.example.gaticosapi_agustinkohan.ui.model

data class CatUIModel(
    val id : String,
    val name : String,
    val temperament : String,
    val countryCode : String,
    val description : String,
    val wikipedia_url : String
)
