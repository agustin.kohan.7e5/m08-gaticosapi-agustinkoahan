package com.example.gaticosapi_agustinkohan.ui.screens


import android.content.Intent
import android.widget.Toast
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ExpandLess
import androidx.compose.material.icons.filled.ExpandMore
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.gaticosapi_agustinkohan.CatDetail
import com.example.gaticosapi_agustinkohan.R
import com.example.gaticosapi_agustinkohan.data.model.Breeds
import com.example.gaticosapi_agustinkohan.ui.theme.Typography
import com.example.gaticosapi_agustinkohan.ui.theme.GaticosAPI_AgustinKohanTheme
import com.skydoves.landscapist.ImageOptions
import com.skydoves.landscapist.glide.GlideImage
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import org.w3c.dom.Text

@Composable
fun MainScreen(
    catsUiState: CatsUiState,
    setSelectCountry: (String) -> Unit,
    modifier: Modifier = Modifier
) {
    when (catsUiState) {
        is CatsUiState.Loading -> LoadingScreen(modifier)
        is CatsUiState.Success -> ResultScreen(
            catsUiState.breeds,
            catsUiState.SelectCountry,
            setSelectCountry,
            modifier
        )
        is CatsUiState.Error -> ErrorScreen(modifier)
    }

}

/**
 * The home screen displaying the loading message.
 */
@Composable
fun LoadingScreen(modifier: Modifier = Modifier) {
    Box(
        contentAlignment = Alignment.Center,
        modifier = modifier.fillMaxSize()
    ) {
        Image(
            modifier = Modifier.size(200.dp),
            painter = painterResource(R.drawable.catface),
            contentDescription = stringResource(R.string.loading)
        )
    }
}

/**
 * The home screen displaying error message
 */
@Composable
fun ErrorScreen(modifier: Modifier = Modifier) {
    Box(
        contentAlignment = Alignment.Center,
        modifier = modifier.fillMaxSize()
    ) {
        Text(stringResource(R.string.error_loading))
    }
}

/**
 * The home screen displaying number of retrieved Images.
 */

@Composable
fun ResultScreen(
    gatitosInfo: List<Breeds>,
    selectCountry: String,
    setSelectCountry: (String) -> Unit,
    modifier: Modifier = Modifier
) {
    Column(
        modifier = Modifier
            .background(color = MaterialTheme.colors.primary)
    ) {
        var catsFiltredInfo = gatitosInfo
        SeleccionCountry(selectCountry, setSelectCountry)
        if (selectCountry != "SelectCountry") {
            catsFiltredInfo = gatitosInfo.filter { it.countryCode == selectCountry }

        }
        Spacer(modifier = Modifier.height(2.dp))
        CatsList(catsFiltredInfo)
    }
}

@Composable
fun SeleccionCountry(selectCountry: String, setSelectCountry: (String) -> Unit) {
    val listItems =
        arrayOf(
            "SelectCountry",
            "EG",
            "US",
            "CA",
            "ES",
            "BR",
            "GR",
            "AE",
            "AU",
            "FR",
            "GB",
            "MM",
            "CY",
            "RU",
            "CN",
            "JP",
            "TH",
            "IM",
            "NO",
            "IR",
            "SP",
            "SO",
            "TR"
        )
    val contextForToast = LocalContext.current.applicationContext

    // state of the menu
    var expanded by remember {
        mutableStateOf(false)
    }

    Row(
        modifier = Modifier
            .fillMaxWidth()
            .background(color = MaterialTheme.colors.primary)
            .padding(5.dp),
        verticalAlignment = Alignment.Top,
        horizontalArrangement = Arrangement.Start,

        ) {

        SelectCountryButton(
            expanded = expanded,
            onClick = { expanded = !expanded },
            selectCountry = selectCountry,
        )

        // drop down menu
        DropdownMenu(
            expanded = expanded,
            onDismissRequest = {
                expanded = false
            },
            modifier = Modifier.height(250.dp)
        ) {
            // adding items
            listItems.forEach { itemValue ->
                DropdownMenuItem(
                    onClick = {
                        setSelectCountry(itemValue)
                        Toast.makeText(contextForToast, "Country: $itemValue", Toast.LENGTH_SHORT)
                            .show()
                        expanded = false
                    },
                    enabled = (itemValue != selectCountry)
                ) {
                    Text(text = itemValue)
                }
            }
        }

    }
}

@Composable
fun SelectCountryButton(
    expanded: Boolean,
    onClick: () -> Unit,
    selectCountry: String,
    modifier: Modifier = Modifier

) {
    Button(
        onClick = onClick,
        colors = ButtonDefaults.buttonColors(backgroundColor = MaterialTheme.colors.secondary)
    ) {
        Text(
            text = selectCountry,
            style = Typography.button,
            color = MaterialTheme.colors.primaryVariant,
            textAlign = TextAlign.Center
        )
        Icon(
            imageVector = if (expanded) Icons.Filled.ExpandLess else Icons.Filled.ExpandMore,
            tint = MaterialTheme.colors.primaryVariant,
            contentDescription = "IconButton"
        )
    }
}

@Composable
fun CatsList(catsList: List<Breeds>, modifier: Modifier = Modifier) {
    val mContext = LocalContext.current
    LazyColumn(modifier = modifier) {
        items(items = catsList) { breeds ->
            Button(
                onClick = {
                    val intent = Intent(mContext, CatDetail::class.java)
                    val catInfoStr = Json.encodeToString(breeds)
                    intent.putExtra("CatInfo", catInfoStr)
                    mContext.startActivity(intent)
                },
                colors = ButtonDefaults.buttonColors(backgroundColor = MaterialTheme.colors.primary)
            ) {

                CatCard(
                    breeds = breeds,
                )
            }
        }
    }
}

@Composable
fun CatCard(breeds: Breeds, modifier: Modifier = Modifier) {
    Card(
        modifier = modifier,
        border = BorderStroke(
            4.dp,
            color = MaterialTheme.colors.secondary
        ),
        elevation = 5.dp,
        backgroundColor = MaterialTheme.colors.background
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(5.dp),
            horizontalArrangement = Arrangement.Start,
            verticalAlignment = Alignment.CenterVertically,

            ) {
            GlideImage(
                imageModel = { breeds.image!!.url }, modifier = Modifier
                    .size(200.dp)
                    .border(BorderStroke(2.dp, MaterialTheme.colors.primaryVariant)),
                imageOptions = ImageOptions(contentScale = ContentScale.Crop)
            )
            Spacer(modifier = Modifier.width(8.dp))
            Column() {
                /*Text(
                    style = Typography.body2,
                    text = "Breed:",
                    color = MaterialTheme.colors.secondary
                )*/
                breeds.name?.let {
                    Text(
                        style = Typography.h5,
                        text = it,
                        color = MaterialTheme.colors.secondary
                    )
                }

                breeds.description?.let {
                    Text(
                        style = Typography.body2,
                        text = it,
                        color = MaterialTheme.colors.primary,
                        overflow = TextOverflow.Ellipsis,
                        maxLines = 2
                    )
                }

            }

        }
    }
}

//Previews
@Preview(showBackground = true)
@Composable
fun LoadingScreenPreview() {
    GaticosAPI_AgustinKohanTheme() {
        LoadingScreen()
    }
}

@Preview(showBackground = true)
@Composable
fun ErrorScreenPreview() {
    GaticosAPI_AgustinKohanTheme() {
        ErrorScreen()
    }
}

@Preview(showBackground = true)
@Composable
fun ResultScreenPreview() {
    GaticosAPI_AgustinKohanTheme() {
        //ResultScreen(images = PreviewData().loadCats())
    }
}

@Preview
@Composable
private fun CatCardPreview() {
    // TODO 2. Preview your card
    /*  CatCard(
          catIMage
           =  PreviewData().loadCats().first()
      )*/

}

