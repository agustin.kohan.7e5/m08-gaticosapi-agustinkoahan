package com.example.gaticosapi_agustinkohan.ui.model.mapper

import com.example.gaticosapi_agustinkohan.ui.model.CatUIModel

/*
class CatDtoUIModelMapper {
    fun map(listDto: List<CatDto>): List<CatUIModel> {
        return mutableListOf<CatUIModel>().apply {
            listDto.forEach {
                this.add(
                    CatUIModel(
                        id = it.id,
                        name = it.name,
                        temperament = it.temperament,
                        countryCode = it.countryCode,
                        description = it.description,
                        wikipedia_url = it.wikipedia_url
                    )
                )
            }
        }
    }
}
 */