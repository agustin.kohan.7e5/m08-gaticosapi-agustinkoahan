package com.example.gaticosapi_agustinkohan.network

import com.example.gaticosapi_agustinkohan.data.model.Breeds
import com.example.gaticosapi_agustinkohan.data.model.CatImage
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import kotlinx.serialization.json.Json
import okhttp3.MediaType.Companion.toMediaType
import retrofit2.Retrofit
import retrofit2.http.GET
import retrofit2.http.Headers

private const val BASE_URL = "https://api.thecatapi.com/v1/"

private const val token = "live_9zm8rozdyUDgRi3S4iWIGGrspLFq9z7tFxwPuOMImY4MUKewp2XlfWdHHe0pQyib"

private val jsonIgnored = Json { ignoreUnknownKeys = true }

private val retrofit = Retrofit.Builder()
    .addConverterFactory(jsonIgnored.asConverterFactory("application/json".toMediaType()))
    .baseUrl(BASE_URL)
    .build()

interface CatsApiService {
    @Headers("x-api-key: $token")
    @GET("breeds")

    suspend fun getPhotos(): List<Breeds>
}


object CatsApi {
    val retrofitService: CatsApiService by lazy {
        retrofit.create(CatsApiService::class.java)
    }
}
