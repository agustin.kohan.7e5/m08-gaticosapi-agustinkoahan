package com.example.gaticosapi_agustinkohan.data.model

import kotlinx.serialization.Serializable
import kotlinx.serialization.SerialName

@Serializable
data class Breeds(

    @SerialName("weight") var weight: Weight? = Weight(),
    @SerialName("id") var id: String? = null,
    @SerialName("name") var name: String? = null,
    @SerialName("temperament") var temperament: String? = null,
    @SerialName("origin") var origin: String? = null,
    @SerialName("country_codes") var countryCodes: String? = null,
    @SerialName("country_code") var countryCode: String? = null,
    @SerialName("description") var description: String? = null,
    @SerialName("life_span") var lifeSpan: String? = null,
    @SerialName("wikipedia_url") var wikipediaUrl: String? = null,
    @SerialName("image") var image: CatImage? = CatImage()

)

