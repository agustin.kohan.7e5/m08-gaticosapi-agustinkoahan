package com.example.gaticosapi_agustinkohan.data.model

import kotlinx.serialization.Serializable
import kotlinx.serialization.SerialName

@Serializable
data class CatImage(
    @SerialName("id") var id: String? = null,
    @SerialName("url") var url: String? = null,
    @SerialName("width") var width: Int? = null,
    @SerialName("height") var height: Int? = null
)
