package com.example.gaticosapi_agustinkohan

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import com.example.gaticosapi_agustinkohan.ui.CatImageApp
import com.example.gaticosapi_agustinkohan.ui.theme.GaticosAPI_AgustinKohanTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            GaticosAPI_AgustinKohanTheme() {
                CatImageApp()
            }
        }
    }
}
