package com.example.gaticosapi_agustinkohan


import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.gaticosapi_agustinkohan.ui.theme.GaticosAPI_AgustinKohanTheme
import com.skydoves.landscapist.ImageOptions
import com.skydoves.landscapist.glide.GlideImage

class LoginActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            GaticosAPI_AgustinKohanTheme() {
                LoginPage()
            }
        }
    }
}

@Composable
fun LoginPage() {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(color = MaterialTheme.colors.primary)
            .verticalScroll(rememberScrollState()),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally,

        ) {

        val username = remember { mutableStateOf(TextFieldValue()) }
        val password = remember { mutableStateOf(TextFieldValue()) }
        val mContext = LocalContext.current


        GlideImage(
            imageModel = { R.drawable.gatopanzon }, modifier = Modifier
                .size(200.dp)
                .border(BorderStroke(2.dp, MaterialTheme.colors.primaryVariant)),
            imageOptions = ImageOptions(contentScale = ContentScale.Crop)
        )

        Spacer(modifier = Modifier.height(20.dp))

        TextField(
            label = { Text(text = "Nombre") },
            value = username.value,
            onValueChange = { username.value = it },
            colors = TextFieldDefaults.textFieldColors(backgroundColor = MaterialTheme.colors.secondary)
        )

        Spacer(modifier = Modifier.height(20.dp))

        TextField(
            label = { Text(text = "Contraseña", color = MaterialTheme.colors.primaryVariant) },
            value = password.value,
            visualTransformation = PasswordVisualTransformation(),
            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Password),
            onValueChange = { password.value = it },
            colors = TextFieldDefaults.textFieldColors(backgroundColor = MaterialTheme.colors.secondaryVariant)
        )

        Spacer(modifier = Modifier.height(20.dp))

        Box(
            modifier = Modifier
                .padding(60.dp)
        ) {
            Button(
                onClick = {
                    if (username.value.text != "" && password.value.text != "")
                        mContext.startActivity(Intent(mContext, MainActivity::class.java))
                    else
                        Toast.makeText(mContext, "Datos incorrectos", Toast.LENGTH_SHORT).show()
                },
                shape = RoundedCornerShape(50.dp),
                modifier = Modifier
                    .fillMaxWidth()
                    .height(50.dp),
                colors = ButtonDefaults.buttonColors(backgroundColor = MaterialTheme.colors.secondary)
            ) {
                Text(text = "Entrar")
            }
        }
    }
}
